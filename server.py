#Ana Villanueva
# !/usr/bin/python3
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import socketserver
import sys
import json
import time


class SIPRegisterHandler(socketserver.DatagramRequestHandler):
    """
    SIP server class
    """
    diccionario = {}

    def putUser(self, dirSip, timeExpires):
        ipClient, puertoClient = self.client_address
        self.diccionario[dirSip] = {'address': ipClient, 'expires': timeExpires}
        print(self.diccionario)
        self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')

    def deleteUser(self, dirSip):
        try:
            del self.diccionario[dirSip]
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        except KeyError:
            self.wfile.write(b'SIP/2.0 200 OK\r\n\r\n')
        print(self.diccionario)

    def register2json(self, dirSip, timeExpires):
        with open("registred.json", 'w') as f:
            json.dump(self.diccionario, f, indent=4)

    def json2register(self, dirSip, timeExpires):
        try:
            with open('registered.json', 'r') as file:
                self.diccionario = json.load(file)
        except (FileNotFoundError):
            pass

    def caducidadExpires(self, dirSip, timeExpires):
        listaUsuarios = list(self.diccionario)
        for usuario in listaUsuarios:
            timeExpires = self.diccionario[usuario]["expires"]
            print(timeExpires)
            tiempoReal = time.strftime('%Y-%m-%d %H:%M:%S +0000', time.localtime())
            print(tiempoReal)
            if timeExpires < tiempoReal:
                del self.diccionario[usuario]
        self.register2json(dirSip, timeExpires)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        global dirSip

        for line in self.rfile:
            messageSplit = line.decode('utf-8')
            if messageSplit != '\r\n':
                print("El cliente nos manda ", messageSplit)
                dataClient = messageSplit.split()
                if dataClient[0] == 'REGISTER':
                    dirSip = dataClient[1].split(':')[1]
                elif dataClient[-2] == 'Expires:':
                    timeExpires = int(dataClient[-1])

                    if timeExpires == 0:
                        self.deleteUser(dirSip)
                    elif timeExpires > 0:
                        timeExpires = timeExpires + time.time()
                        timeExpires = time.strftime('%Y-%m-%d %H:%M:%S +0000', time.localtime(timeExpires))
                        self.putUser(dirSip, timeExpires)

                    self.caducidadExpires(dirSip, timeExpires)


if __name__ == "__main__":
    # Listens at localhost ('') port 6001
    # and calls the EchoHandler class to manage the request
    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
