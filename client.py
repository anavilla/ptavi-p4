#!/usr/bin/python3
"""
Programa cliente UDP que abre un socket a un servidor
"""

import socket
import sys

""" Indica el número de parametros que se pueden poner por el terminal """
if len(sys.argv) != 6:
    sys.exit("Usage: client.py puerto register sip:addres expires_value")
if str(sys.argv[3]) != "register":
    sys.exit("Usage: client.py puerto register sip:addres expires_value")

SERVER = sys.argv[1]
PORT = int(sys.argv[2])
USER = str(sys.argv[4])
EXPIRE = int(sys.argv[5])


if sys.argv[3] == "register":
    LINE = str("REGISTER " + USER + " SIP/2.0\r\n\r\n " + "Expires: " + str(EXPIRE))
else:
    LINE = sys.argv[3:]

# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.connect((SERVER, PORT))
    print("Enviando:", LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)
    print('Recibido -- ', data.decode('utf-8'))

print("Socket terminado.")

